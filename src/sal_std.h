#ifndef SAL_STD_H_
#define SAL_STD_H_

#define SAL_STD_IMPLEMENTATION
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

// Constants
#define SAL_STRING_BASE_CAP 1024

// Memory management
#define SAL_MALLOC malloc
#define SAL_REALLOC realloc
#define SAL_CALLOC calloc
#define SAL_FREE free

// utility
#define sal_has_error(val) sal_has_error_generic((SalErrorField*)&val.error)
#define sal_str_from_cstr(str) sal_str_from_cstr_(str, sizeof(str) - 1)
#define sal_new_string() sal_new_string_with_size(SAL_STRING_BASE_CAP)
#define SAL_FILL_RESULT_WITH_ERROR(res, err_val) \
        res.error.has_value = false; \
        res.value.size = 0; \
        res.error.error = err_val
#define SAL_FILL_RESULT_WITH_VALUE(res, val) \
        res.error.has_value = true; \
        res.value = val

// Sal types
typedef struct
{
    char* buf;
    size_t size;
} SalStr;

typedef enum
{
    SAL_COULD_NOT_OPEN_FILE,
    SAL_NOT_ENOUGH_MEMORY,
    SAL_STRING_NOT_FOUND,
    SAL_NEEDLE_BIGGER_THAN_HAYSTACK,
    SAL_STRING_SHRINK_ERROR,
} SalError;

#define SAL_DEFINE_ERROR_STR(val) [val] = { #val, sizeof(#val)}
const SalStr sal_error_strs[] = {
    SAL_DEFINE_ERROR_STR(SAL_COULD_NOT_OPEN_FILE),
    SAL_DEFINE_ERROR_STR(SAL_NOT_ENOUGH_MEMORY),
    SAL_DEFINE_ERROR_STR(SAL_STRING_NOT_FOUND),
    SAL_DEFINE_ERROR_STR(SAL_NEEDLE_BIGGER_THAN_HAYSTACK),
    SAL_DEFINE_ERROR_STR(SAL_STRING_SHRINK_ERROR),
};
 
typedef struct
{
    char* buf;
    size_t size;
    size_t capacity;
} SalString;

typedef struct
{
    bool has_value;
    SalError error;
} SalErrorField;

typedef struct
{
    SalErrorField error;
    SalStr value;
} SalStrResult;

typedef struct
{
    SalErrorField error;
    SalString value;
} SalStringResult;

// public functions
SalStr sal_str_from_cstr_(char* str, size_t size);
bool sal_has_error_generic(SalErrorField* r);
SalStr sal_get_error_str(SalErrorField error);
SalStr sal_to_str(SalString s);
SalStringResult sal_new_string_with_size(size_t size);
SalStringResult sal_string_append(SalString* string, SalStr str);
SalStringResult sal_shrink_string(SalString* s);
SalStringResult sal_read_file(const char* file_name);
void sal_print_str(SalStr f);
void sal_debug_string(SalString f);
void sal_delete_string(SalString* f);
SalStr sal_get_line_with_rest(SalStr str, SalStr* rest);
SalStr sal_get_line(SalStr str);
bool sal_is_empty(SalStr str);
bool sal_is_equal(SalStr s1, SalStr s2);
SalStrResult sal_find_str_with_rest(SalStr haystack, SalStr needle, SalStr* rest);
SalStrResult sal_find_str(SalStr haystack, SalStr needle);
SalStrResult sal_split_delim_with_rest(SalStr dest, SalStr delim, SalStr* rest);
SalStrResult sal_split_delim(SalStr dest, SalStr delim);
SalStringResult sal_copy_to_string(SalString* dest_string, SalStr str);
SalStringResult sal_to_string(SalStr s);

#ifdef SAL_STD_IMPLEMENTATION

SalStr sal_str_from_cstr_(char* str, size_t size)
{
    SalStr res = { .buf = str, .size = size };
    return res;
}

bool sal_has_error_generic(SalErrorField* r)
{
    return !r->has_value;
}

SalStr sal_get_error_str(SalErrorField error)
{
    return sal_error_strs[error.error];
}

SalStr sal_to_str(SalString s)
{
    SalStr res;
    res.size = s.size;
    res.buf = s.buf;
    return res;
}

SalStringResult sal_new_string_with_size(size_t size)
{
    SalStringResult res;
    size_t cap = SAL_STRING_BASE_CAP;

    while (size > cap)
        cap *= 2;

    //res.value.buf = SAL_CALLOC(1, cap);
    res.value.buf = SAL_MALLOC(cap);
    if(res.value.buf == NULL)
    {
        SAL_FILL_RESULT_WITH_ERROR(res, SAL_NOT_ENOUGH_MEMORY);
        return res;
    }

    res.error.has_value = true;
    res.value.capacity = cap;
    res.value.size = size;
    return res;
}

static SalStringResult grow_string(SalString* s, size_t min_size)
{
    size_t new_cap = s->capacity * 2;
    if(new_cap <= 0) new_cap = SAL_STRING_BASE_CAP;
    SalStringResult res;
    while(min_size > new_cap)
    {
        new_cap *= 2;
    }

    char* new_buf;
    if(s->buf !=  NULL)
    {
        new_buf = SAL_REALLOC(s->buf, new_cap);
    }
    else
    {
        //new_buf = SAL_CALLOC(1, new_cap);
        new_buf = SAL_MALLOC(new_cap);
    }
    
    if(new_buf == 0)
    {
        SAL_FILL_RESULT_WITH_ERROR(res, SAL_NOT_ENOUGH_MEMORY);
        return res;
    }

    s->buf = new_buf;
    s->capacity = new_cap;

    SAL_FILL_RESULT_WITH_VALUE(res, *s);
    return res;
}

static SalStringResult grow_string_if_needed(SalString* string, size_t min_size)
{
    SalStringResult res;
    if(string->capacity < min_size)
    {
        res = grow_string(string, min_size);
    }
    else 
    {
        SAL_FILL_RESULT_WITH_VALUE(res, *string);
    }

    return res;
}

SalStringResult sal_string_append(SalString* string, SalStr str)
{
    SalStringResult res;
    size_t needed_size = string->size + str.size;

    res = grow_string_if_needed(string, needed_size);
    if(sal_has_error(res))
    {
        return res; 
    }
    memcpy(string->buf + string->size, str.buf, str.size);
    string->size += str.size;

    SAL_FILL_RESULT_WITH_VALUE(res, *string);
    return res;
}

// is this really needed ?
SalStringResult sal_shrink_string(SalString* s)
{
    SalStringResult res;
    if(s->capacity <= s->size)
    {
        SAL_FILL_RESULT_WITH_VALUE(res, *s);
        return res;
    }
    char* new_buf;
    new_buf = SAL_REALLOC(s->buf, s->size);

    if(new_buf == NULL)
    {
        SAL_FILL_RESULT_WITH_ERROR(res, SAL_STRING_SHRINK_ERROR);
        return res;
    }

    assert(false && "not implemented yet!");
}

SalStringResult sal_read_file(const char* file_name)
{
    FILE* f = fopen(file_name, "r");
    SalStringResult res;

    if (f == NULL)
    {
        SAL_FILL_RESULT_WITH_ERROR(res, SAL_COULD_NOT_OPEN_FILE);
        return res;
    }

    fseek(f, 0, SEEK_END);
    res.value.size = ftell(f);
    fseek(f, 0, SEEK_SET);

    res = sal_new_string_with_size(res.value.size);
    if (sal_has_error(res))
    {
        return res;
    }

    fread(res.value.buf, res.value.size, 1, f);
    res.error.has_value = true;
    fclose(f);

    return res;
}

void sal_print_str(SalStr f)
{
    if (f.buf && f.size) printf("%.*s\n", (int)f.size, f.buf);
}

void sal_debug_string(SalString f)
{
    printf("===================\n");
    printf("buf: %p | size: %zu | cap: %zu\n", f.buf, f.size, f.capacity);
    sal_print_str(sal_to_str(f));
    printf("===================\n");
}

void sal_delete_string(SalString* f)
{
    if (f && f->buf)
    {
        SAL_FREE(f->buf);
        f->buf = 0;
        f->size = 0;
    }
}

SalStr sal_get_line_with_rest(SalStr str, SalStr* rest)
{
    SalStr res;
    res.buf = str.buf;
    uint32_t i;
    for(i = 0; i < str.size; i++)
    {
        if(str.buf[i] == '\n')
        {
            break;
        }
    }
    res.size = i;

    if(rest)
    {
        rest->buf = res.buf + res.size + 1;
        rest->size = str.size - i;
    }

    return res;
}

SalStr sal_get_line(SalStr str)
{
    return sal_get_line_with_rest(str, NULL);
}

bool sal_is_empty(SalStr str)
{
    return str.size == 0;
}

bool sal_is_equal(SalStr s1, SalStr s2)
{
    size_t min_size = s1.size < s2.size ? s1.size : s2.size;
    for(size_t i = 0; i < min_size; ++i)
    {
        if(s1.buf[i] != s2.buf[i])
        {
            return false;
        } 
    }

    return true;
}

SalStrResult sal_find_str_with_rest(SalStr haystack, SalStr needle, SalStr* rest)
{
    SalStrResult res;
    if(needle.size > haystack.size)
    {
        SAL_FILL_RESULT_WITH_ERROR(res, SAL_NEEDLE_BIGGER_THAN_HAYSTACK);
        return res;
    }

    SalStr found = {
        .buf = haystack.buf,
        .size = needle.size,
    };

    while((haystack.buf + haystack.size) - (found.buf) >= (long)needle.size)
    {
        if(sal_is_equal(found, needle))
        {
            res.error.has_value = true;
            res.value = found;
            
            if(rest != NULL)
            {
                rest->size = (haystack.buf + haystack.size) - (found.buf + needle.size);
                rest->buf = found.buf + needle.size;
            }

            return res;
        }
        found.buf++;
    }

    // if no str found return empty str 
    // because we research all haystack
    if(rest != NULL)
    {
        rest->size = 0;
        rest->buf = haystack.buf + haystack.size;
    }

    SAL_FILL_RESULT_WITH_ERROR(res, SAL_STRING_NOT_FOUND);
    return res;
}

SalStrResult sal_find_str(SalStr haystack, SalStr needle)
{
    return sal_find_str_with_rest(haystack, needle, NULL);
}

SalStrResult sal_split_delim_with_rest(SalStr dest, SalStr delim, SalStr* rest)
{
    static SalStr last_res = {0};
    SalStrResult res;
    SalStrResult first_delim = sal_find_str(dest, delim);
    if(sal_has_error(first_delim))
    {
        if(dest.buf != last_res.buf && dest.size != last_res.size)
        {
            return first_delim;
        }
        else
        {
            SAL_FILL_RESULT_WITH_VALUE(res, dest);
            last_res.buf = 0;
            last_res.size = 0;
            return res;
        }
    }

    res.error.has_value = true;
    res.value.buf = dest.buf;
    res.value.size = first_delim.value.buf - dest.buf;

    if(rest != NULL)
    {
        size_t rest_token_offset = res.value.size + delim.size;
        rest->buf = dest.buf + rest_token_offset;
        rest->size = dest.size - rest_token_offset;
        last_res = *rest;
    }

    return res;
}

SalStrResult sal_split_delim(SalStr dest, SalStr delim)
{
    return sal_split_delim_with_rest(dest, delim, NULL);
}

SalStringResult sal_copy_to_string(SalString* dest_string, SalStr str)
{
    SalStringResult res;
    res = grow_string_if_needed(dest_string, str.size);

    if(sal_has_error(res))
    {
        return res;
    }

    for(size_t i = 0; i < str.size; ++i)
    {
        dest_string->buf[i] = str.buf[i];
    }

    res.value = *dest_string;
    res.error.has_value = true;
    res.value.size = str.size;

    return res;
}

SalStringResult sal_to_string(SalStr s)
{
    SalString string = {0};
    return sal_copy_to_string(&string, s);
}

#endif // SAL_STD_IMPLEMENTATION
#endif // !SAL_STD_H_

