#include <unistd.h>
#include <stdbool.h>
#include <threads.h>

#include "sal_std.h"

int main(int argc, char* argv[])
{
    (void)argc;
    (void)argv;

    SalStringResult f = sal_read_file("test");
    if(sal_has_error(f))
    {
        sal_print_str(sal_get_error_str(f.error));
        return -1;
    }

    SalStr rest;
    SalStrResult token = sal_split_delim_with_rest(sal_to_str(f.value), sal_str_from_cstr(" "), &rest);

    SalString str = {0};
    sal_string_append(&str, token.value);
    while(!sal_has_error(token))
    {
        sal_print_str(token.value);
        token = sal_split_delim_with_rest(rest, sal_str_from_cstr(" "), &rest);
        sal_string_append(&str, token.value);
    }

    sal_debug_string(str);

    sal_delete_string(&f.value);
    return 0;
}
